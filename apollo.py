import os.path
import pafy
import sys

DOWNLOAD_FOLDER="./download"

def download(url):
    print("setting url...")
    vid = pafy.new(url)
    print("getting audio stream...")
    stream = vid.getbestaudio()
    filename = stream.title + "." + stream.extension
    full_path = DOWNLOAD_FOLDER + "/" + filename

    if os.path.isfile(full_path):
        print("file %s already exists. Skipping download" % filename)
        return -1

    print("downloading...")
    stream.download(filepath=full_path)
    print("finished - %s received" % stream.get_filesize())

if not os.path.isdir(DOWNLOAD_FOLDER):
    os.mkdir(DOWNLOAD_FOLDER)
    print("generating new download folder")

if len(sys.argv) != 2:
    raise ValueError('Please provide url to download')
else:
    url = sys.argv[1]
    download(url)
