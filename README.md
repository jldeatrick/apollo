# apollo
youtube url-based music download tool

## Prerequesites
- python 3
- pip3
- Pafy
- youtube-dl

## Installation

#### pip3
`sudo apt-get install pip3`

#### Pafy
`pip3 install pafy`

#### youtube-dl
`pip3 install youtube_dl`

## Usage
`python3 apollo.py <url>`
